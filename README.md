# Development Guidelines

This repository serves to collect the development guidelines that are common throughout our projects.

As it continues to grow, it will get subfolders covering more detailed aspects.

## React Automated Testing

Kindly visit below URLs for more information about Testing React Components:

- [Basics about testing React app](react-component-testing-fundamental.md)
- [Various automated testing frameworks](react-automated-testing-frameworks.md)
- [Installation guide for Jest and Enzyme](setup-install-guide-jest-enzyme.md)
- [Steps for full test coverage](react-full-test-coverage-steps.md)
