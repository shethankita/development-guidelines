# Full test coverage steps for React component testing

Below are the steps to do automated testing in existing project:

- Based on project structure, identify the correct order of components to test
- Define what not to test (what to omit)
- Main instructions for component testing

Behind each test our motive is to achieve 100% test coverage and sometimes we are unable to achieve it if we don’t know how to do proper testing for an existing React application. Below guideline may helpful to achieve full test coverage:

## **Define the correct order of components based on project structure**

Consider below is the project structure for example:

![Project structure](images/project-structure.png)

**Shared** directory is the most important part of any project as it consists of the components that are used in several different pages. They are reusable, small and not complex. If one or another component fails, it will cause failing in other places. That’s the reason we must test it first in order.

We can categories components into below structure.

![Define queue for test coverage](images/test-covergae-queue.png)

**Thumb rule:** Always follow the rule from simple to complex.

In Shared directory, we can define order of testing component is as below:

- **Independent components –** Forms/Inputs folder

As name suggests, their rendering does not depend on the other components and can be used as single isolated unit. From the above structure, input directory from forms folder are Independent components. It contains input components such as Text, CheckBox, DatePicker, SelectInput, etc.

- **Auxiliary components –** Utils (utility) directory

These components are often used in input components but should be tested apart from them. Components in this Utils folder are not complicated but very important. They are frequently reusable and help with repeated actions.

- **Simple functionality components –** Widgets directory

Widgets are the little components with simple functionality.

- **Complex components –** Modals, HOC directory and Forms/Fields folder

Complex components such as Modals can be used independently or in conjunction with other components. Next, need to take the directory from which components have already been used in tested components. Thus, component from HOC directory was present in Widgets component. And the last one is the Fields folder which contains components connected to with redux forms.

Final components order based on our structure will look like this:

![Component structure](images/component-order.png)

Of course, the structure of your project can differ; it can have other directory names or can have additional components, actions, and reducers, but the logic of defining the order for testing the components is the same.

## Define what should be omitted in test coverage

- **Third party libraries –** Don’t test functionality that is taken from another library, you are not responsible for that code, just skip it.

- **Constants –** As name suggests, they are not changeable. It is a set of static code that is not intended to vary.

- **Inline styles –** inline styles don’t change component’s behavior; consequently, they shouldn’t be tested. There can be an exception in case your styles change dynamically.

- **Things not related to the tested component –** Don’t test wrapper, just analyze and test them separately. We should skip test components that were imported in the tested component.

Correct testing approach

- Snapshot testing
- Component logic testing

## Main instructions for component testing

- **Create snapshot first -** One component should have only one snapshot.

- **Testing props –** Two ways to test props:
First check the defaultProps value and second check the custom value of the prop after render.

- **Testing propTypes for value (data types of props) –** It is important to test what type of data comes in the props or what kind of data is obtained after certain actions. Data type is a very important programming part and shouldn’t be skipped.

- **Testing events -** You can check event in several ways; the most widely used are:

    mock event => simulate it => expect event was called

    mock event => simulate event with params => expect event was called with passed params

    pass necessary props => render component => simulate event => expect a certain behavior on called event

- **Testing conditions –** There are certain cases where your rendered output is based on some conditions. Do not forget to test this because with default values, only one branch will pass the test, while the second one will remain untested.

- **Testing states’ –** Two ways to test states’:
First one checks current state and second one checks state after calling event.
