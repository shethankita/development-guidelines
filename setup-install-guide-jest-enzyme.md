# Setup and Install

To work with Jest and Enzyme, in the terminal or command prompt, install the following dependencies to your existing create-react-app project.

- _npm install enzyme enzyme-adapter-react-16 enzyme-to-json --save-dev_

**Note:** we do not need to install Jest, as CRA is bundled with Jest out of the box.

## Configuration

To configure Jest & Enzyme to work together, create a new file called ***setupTests.js*** in the src/ directory.

    import { configure } from 'enzyme';
    import Adapter from 'enzyme-adapter-react-16';
    configure({ adapter: new Adapter() });

In the **_package.json_** file, add the following Jest configuration:

    "jest": {
      "snapshotSerializers": ["enzyme-to-json/serializer"],
      "collectCoverageFrom": ["src/**/*.js", "!src/index.js"],
      "coverageReporters": ["text"]
    }

// test file

    import { shallow, mount, render } from 'enzyme';
    const wrapper = shallow(`<Foo />`);

**TIP:** you can get method autocomplete for Jest by installing its type definitions with:

    npm i @types/jest --save-dev

For Snapshot testing, you need to install package - React Test Renderer:

    npm install react-test-renderer

[For more detail, click here!](https://enzymejs.github.io/enzyme/docs/installation/)
