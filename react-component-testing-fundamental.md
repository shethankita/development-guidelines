# ReactJS - Automated component testing

## Why it is important to build an automated test script

Till now, we all are developing back-end app with some architecture like MVC or any other and testing of this app is easy. We take our language of choice, we built it with our selected framework, write some tests and hit "run". Life is simple and great.

And suddenly, we all switched to front-end MVC. And this React is nothing but View part of MVC. Our testable backend became glorified database servers. Our most complicated code moved into the browser. Therefore, our apps were no longer testable in practice because testing front-end code and UI component is little bit hard. Therefore, idea of implementing Automated test script for UI component with TDD approach came into picture.

## What is TDD approach and importance of it

Any piece of code that has no tests is said to be legacy code. Every component lacking unit tests has legacy code that becomes difficult to maintain. Therefore, one of the best ways to avoid creating legacy code is using **Test Driven Development**.

TDD – as its name describes is based on **TEST first approach**. In general software development process, you are writing unit test after production code is written whereas in TDD, you are writing unit test before production code starts. It makes the developer focus on the requirements before writing the code, a subtle but important difference.

Test-driven development is a software development process that relies on the small rapid iterations of the test cycles. The main intension behind TDD approach is to write **clear and bug-free code**.

TDD brings many benefits to code -

- It enables easy code refactoring while keeping your code clean and functional.

- You have a clear picture in your mind of what you are trying to build before writing actual code.

- It enables developers to write small test codes which are easy to debug.

- High test coverage.

To implement TDD is weird at first because in that you must write tests first. But by creating tests first, you get a confidence covering each logic scenario in your component, which would make it easy to refactor and maintain. Also, you can add unit tests after production code, but in that case you may found risk of overlooking some scenarios which will cause the issue at production stage. But bottom line should be test code necessary to fail and production code necessary to pass.

As a developer, you need to write tests for every small functionality of the application. It is a cyclic process and each cycle starts by writing a unit test.

TDD can easily be compiled in **four easy steps:**

- **Write a test –** Practice yourself by writing a test for every small functionality or possible challenge. This first step is tricky because here developer is not writing any proper code rather s/he just tries to cover each expected requirements and exception conditions based on use cases or user stories.

    **Focus –** to create boilerplate for each and every small functionality and conditions.

- **Run the test and Write the code –** Since there is no code, test should fail for the expected reason, but it improves developer's confidence. And developer will start writing the minimum code required to pass the test.

    **Focus –** to write code only based on step 1.

- **Refactor code to clean –** Improvise on the code quality. Developer will refactor the code to ensure functionality is intact and code is refined and maintainable.

    **Focus –** to ensure code quality, no duplication.

- **Repeat the process –** Now repeat this process for every new feature you introduce in the application.

TDD must be adopted by every developer as it increases productivity and also improves the code quality. Applications developed using TDD technique are more modular and flexible.

Particularly, in React apps, code gets filled up with a lot of complex conditions due to service calls and change of state. But a predictable test environment, multiple test runners, assertion libraries and proper testing frameworks will make developer's life easy.

## Types of testing in ReactJS

Testing ensures the quality of what we are developing. We can test React components similar to testing other JavaScript code. There are two ways to test React components.

- Rendering component trees **(Unit and Integration testing)** – in a simplified test environment and asserting on their output.
- Running a complete app **(End-to-End testing)** – in a realistic browser environment which is known as "end-to-end" test.

Automated testing can be grouped into a testing pyramid as depicted below.

![Test Pyramid](images/test-pyramid.png)

A testing pyramid illustrates when we should use one testing method versus another. The pyramid shows that many unit-snapshot tests can be used to validate one integration test, and many integration tests can be used to validate one manual test. At the peak of the pyramid, we have an end-to-end test: manual testing of the whole application.

There are different approaches when it comes to testing a React app.

- **Unit Tests —** Unit tests helps to check that individual unit of code (mostly functions) work as expected. Testing an isolated part of your application.

Usually done in combination with **shallow rendering**. Eg, a component renders with default props.

- **Integration Tests —** Integration tests are tests where individual units/features of the app are combined and tested as a group. Testing if different parts work or integrate with each other.

Usually done with **mounting or rendering a component**. Eg, if a child component can update context state in a parent.

- **End-to-End Tests** — This test helps to confirm that entire features work from the user’s perspective when using the actual application. Tests are done in a simulated browser, where it combines multiple unit and integration tests into one big test and very little is mocked or stubbed. Eg, testing an entire authentication flow.

**Note: Shallow, Mount and Render are the three methods for rendering React components using Enzyme testing utility.**

For frontend development, start with unit/snapshot testing. Proceed to an integration test only if necessary. Try to avoid manual/UI testing.

To precise, testing in React is a three-step process:
**ARRANGE ==> ACT ==> ASSERT**

Arrange, your app is in a certain original state. Act, then something happens (click event, input, etc.). Then you assert, or make a hypothesis, of the new state of your app. The tests will pass if your hypothesis is correct and fail if it is wrong.

From various JavaScript testing frameworks to useful assertion libraries, choosing the right toolset is the key in leveraging TDD in React, from component unit-testing to integration testing and even end-to-end testing React apps.

## What are different aspects of React component UI testing

A React component combines logic and presentational code. It's mainly used to provide an abstract layer for web and mobile UI components based on state and properties changing. We refer to UI for major two things:

### Structural Testing (Presentational code / Presentational component)

In this, you can focus on structure of the component and how it's laid out.

Eg. We have Login component and for structural testing, we will test whether or not it has following content:

- A title with "Login to website XXX"
- Two inputs – Email and Password
- A submit button
- Two hyperlinks for – Sign up and Forgot Password
- An error screen to display errors

### Interaction Testing (Business logic / Container component)

UI is all about interacting with the user. We do this with bunch of UI elements, such as buttons, links, and many other input elements. With interaction testing, we need to test if they are working properly.

w.r.t. Login component, it should do below things:

- When we click on Submit button, it should give us Username and Password. (some business logic)
- When we click hyperlinks, it should redirect to the respected pages.

So, basically two things to test in React app,

- Given a set of inputs (state & props), assert what a component should output (render).
- Given a user action, assert how the component behaves. The component might make a state update or call a prop-function passed down from a parent.

## Types of React components

Components let you split the UI into independent, reusable pieces, and think about each piece in isolation. Conceptually, components are like JavaScript functions. They accept arbitrary inputs (called “props”) and return React elements describing what should appear on the screen.

Components can refer to other components in their output. A button, a form, a dialog, a screen: in React apps, all those are commonly expressed as components.

A **good thumb rule** is that if a part of your UI is used several times (Button, HyperLink, Panel), or is complex enough on its own (App, Comment, Story), it is a good candidate to be a **reusable component**.

**Some basics about React:**

**Props –** values which we can't change rest of life like button background color is blue so manageable by props.

**State –** whenever you have values that needs to change, must use State.

On mouse hover of button, background color should change then you can do it by state only.

**Use Props for static values and State for dynamic values.**

**React lifecycle – Mounting, Updating (props and state) and Unmounting of React components.**

![React Lifecycle](images/react-lifecycle.png)

React Components are usually re-rendered when:

- setState() is called
- props values are updated
- forceUpdate() is called

When we build ReactJS apps for our clients, we usually use a thin layer between our component representation and the application’s logic. This allows us to decouple the visual representation of the component from its behaviours.

There are **four types of React components** which are,

- **Class components or Stateful –**

Stateful components store information about component's state and provide the necessary context.

Basically, classes that can manipulate state, props and **lifecycle** methods – mounting, updating and unmounting of React component.

- **Functional components or Stateless –**

Stateless components have no memory and cannot give context to other parts of the UI.  
They only receive props (inputs) from parent component and return you JSX elements.  
They are scalable and reusable, and similar to pure function in JavaScript.  
Usually they are used to display information and easy to read, debug and test.

If you ever have a class component with only a render method – you should always make it a functional component.

Basically, **No lifecycle** and it takes In -> Props and Out (render) -> JSX.

- **Pure components –**

It is like better functional component and is used to provide optimization. Re-renders only if state or props is different from previous state or props. This is very useful when you do not want to re-render a certain component while the props and state remain the same.

One major difference between a regular **React.Component** and a **React.PureComponent** is that pure components perform **shallow comparisons** on state change. Pure components take care of shouldComponentUpdate() by itself.

    Pure component (it never changes its own props):
    function sum(a, b) {
        return a + b;
    }

    Impure component (it changes its own input):
    function formula(account, amount) {
        account.total -= amount;
    }

**Strict rule:** All React components must act like pure functions with respect to their props.

- **Higher Order Components (HOC) –**

Functions that return one or multiple components dynamically, depending on array data.

Eg, this.props.myArray.map((element) => (`<MyComponent />`))

- Get/fetch data from state or props (should be an array).
- Map over that array and return a React component for each element.

HOCs are very useful and can find use in almost every React application. Eg, comments.

Components don’t have to emit DOM. They only need to provide composition boundaries between UI concerns.

## Best practices in React

React is a JavaScript library developed by Facebook. Below is the list of React best practices:

- **DRY your code – Don't Repeat Yourself –** Consolidate duplicate code but keep in mind code duplicate is NOT always a bad thing.

- Keep component small and function specific. Each small function can be reused across multiple projects. Avoid having large classes, methods or components, including Reducers.

- Reusability is important, so keep creation of new components to the minimum required. One function should have one component to achieve consistency. If component becomes huge and difficult to maintain, it's better to break it up into as many smaller components as required.

- Attach comments to code only where necessary which will keep code visually clutter free.

- Name a component after the function that it executes so that it's easily recognizable. Eg ShippingTable or Avatar rather UserAvatar and CustomerAvatar.

- Use capitals for component name so that JSX (a JavaScript extension) can identify them differently from default HTML tags.

- Avoid passing too many attributes or arguments. Limit yourself to five props that you pass into your component. Use ReactJS defaultProps and propTypes.

- Separate stateful aspect from rendering. We should keep stateful data-loading logic separates from rendering stateless logic. It's better to have one stateful component to load data and another stateless component to display that data. This reduces the complexity of the components. Eg, if app is fetching some data on mount, then we need to manage data in the main component and pass the complex render task to a sub-component as props.

- Code should execute as expected and be testable. Simple, code should behave as expected and be testable easily and quickly. It’s a good practice to name test files identical to the source files with a .test suffix.

- Write more tests that give more test coverage for your code with a little effort and test code to ensure its proper functioning. Every time you find a bug, make sure you write a test first.

- Use snippet libraries. Code snippets helps you to keep up with the best and most recent syntax. They also help to keep your code relatively bug free.

- Always use a dependency manager with a lock file, such as NPM or yarn.

- Follow linting rules / use linter, break up lines that are too long. Linting is a process wherein we run a program that analyses code for potential errors. Mostly, we use it for language-related issues. But it can also fix many other issues automatically, particularly code style. Using a linter in your React code helps to keep your code relatively error-free and bug-free.

- When using ReduxJS, split your Reducer code into smaller methods to avoid huge JSON within Reducer.

- Use event synchronizer, such as Redux-Thunk, for interactions with your back-end API.

- Name your event handlers with handle prefixes, such as `handleClick()` or `handleUpdate()`. Use `onChange` to control your inputs, such as `onChange={this.handleInputChange}`. Use partial components, such as `<>` … `</>`. Use `map()` to collect and render collections of components. Use ES6 de-structuring for your props. Keep your own jslint configuration file.

- Component hierarchy should be neat and clean. Keep all files related to any one component in a single folder, including styling files. If a large component has its smaller parts then split into sub-folders and keep these smaller components all together within that component folder, this hierarchy will then be easy to understand. This way, we can easily extract code to any other project or even modify the code whenever you want.

![Component Structure](images/component_structure.png)
