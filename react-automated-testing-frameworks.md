# Automated testing frameworks for ReactJS

There seems to be a growing number of JavaScript based libraries, frameworks and test automation tools to test ReactJS applications. Below is the list of it:

## Jest

Jest is an open-source test framework created by Facebook that has a great integration with ReactJS.
Jest provides an integrated "zero-configuration" experience and a command line tool for test execution.
Performance is also a plus with process-based parallel testing and optional priority to failed tests.
Jest is not only a testing library but also comes with its own **test runner** and **assertion functions**. So, if you want you can omit Mocha (test runner) and Chai (assertion library) and use Jest solely with Enzyme.
Jest is probably the most popular testing framework for ReactJS and React Native apps. It also works with many JavaScript projects with NG, VueJS, TypeScrip and even Babel.

**Best features of Jest are UI Snapshot testing and Behaviour testing by Mocking modules, functions or timers.**

## Mocha

Mocha is a JavaScript test framework and a test runner for Node.js programs, featuring browser support, asynchronous testing, test coverage reports, and use of any assertion library.
Just like Jest and other frameworks, Mocha can be combined with Enzyme and also with Chai and other libraries for assertion, mocking etc.
Sometimes it becomes more painful in configuration for common tasks (Snapshot testing) which requires more tools to add in your workflow.
**Mocha** is often used in **React applications**, whereas an alternative such as **Karma** is often used in **Angular applications**.

A **describe** block is used to describe a test suite (e.g. for a component or function) and a **it** block is used for the test itself.

    describe("Header component", () => {
        it("should render without errors", () => {
        })
    })

## Chai

Chai is a BDD / TDD assertion and exception library for Node.js.
It’s often associated with testing in Mocha with Enzyme, and can also be used with Jest and Enzyme.
For Angular, popular alternative would be **Jasmine**.

The simplest assertion you can make in your testing block is

    expect(true).to.equal(true)

## Karma

Karma is not a testing framework or an assertion library (works with Jasmine, Mocha etc).
It launches an HTTP server and generates the test runner HTML file.
With Karma, you can execute JavaScript code in multiple real browsers.
You can run tests locally and check them in real browsers and devices, from your phone to tablets and desktops.
Karma also works with continuous integration to Jenkins, Travis etc.

## Jasmine

Jasmine is a BDD JavaScript testing framework for browsers and Node.js. It's been traditionally used in frameworks like Angular with CLI.
To test React applications, Jasmine can be used along with Babel and Enzyme.

## Enzyme

Enzyme is not a testing framework, but rather a testing utility for React apllications developed by Airbnb.
It makes it easier to assert, manipulate and traverse the output of React components.
You can use it with any test runner like Mocha, Jasmine and mostly used with Chai and nowadays is getting more and more popular.

You can render components in isolation (unit tests) with the shallow function or in their context (integration test) by rendering their child components as well with the mount or render functions. The rendered components can use selectors and other functions to access elements, props or state of the components. In addition, it’s possible to simulate events on buttons or input fields. In short, it can really help you render components, find elements and interact with them.

## React-testing-library

It is a testing library used to simulate user behaviour in your tests. Much like Enzyme, this library is a simple and complete set of React DOM testing utilities aimed to imitate actual user actions and workflows.

## React test utils and test render

It is not a library but rather a collection of useful utilities in React that help to test components using a testing framework of your choice. Test renderer lets you render React components into pure JavaScript objects without depending on the DOM.

## Cypress IO

Cypress is a JavaScript **end-to-end (E2E) testing** framework that makes it easy to setup, write, run and debug tests in the browser.
Because Cypress works in the actual browser, you can use the browser’s dev tools side-by-side to debug tests.
You can capture screenshots and videos with it, mock data from third-party APIs and run it in a continuous integration (CI) environment too.
However, you can’t use Cypress to drive two browsers at the same time, which might hurt.

## Puppeteer

Pupeeter is a Node library which provides an API to control Chrome over the DevTools protocol.
It runs **headless** by default but can be configured to run full (non-headless) Chrome or Chromium.
With the help of Pupeeter, you can do anything you can manually do in the browser, such as generating pre-rendered content from SPAs, generating screenshots, and even automating actions like form submissions, keyboard inputs etc.
Puppeteer is often used with Jest to test React applications end-to-end.

## Bit

Bit isolates and encapsulates React components, making them reusable across your different projects and applications. Bit’s unique ability to virtualluy isolate components from their projects makes it a powerful tool for TDD.

## Sinon

Sinon is a neat library for spies, stubs and mocks. Whenever you have to make sure that a function has been called with certain arguments or whenever you want to return mocked data from an endpoint, Sinon will be your solution to make it happen.

To achieve better TDD, we can create flexible and smooth workflow by combining the right testing framework with the right assertion libraries, from component unit testing to integration testing and even end-to-end testing. With the modularity of React comes better TDD.

**_We will use Jest along with Enzyme for Unit, Integration and Snapshot tests. Later we will use Cypress for End-to-End (E2E) tests. Jest is mainly used for snapshot tests and Enzyme can cover unit and integration tests._**

Both Jest and Enzyme are specifically designed to test React applications. Jest can be used with any other JavaScript app, but Enzyme only works with React. Jest can be used without Enzyme to render components and test with snapshots, Enzyme simply adds additional functionality. Enzyme can be used without Jest; however, Enzyme must be paired with another test runner if Jest is not used.

## About JEST

Jest is a Node-based runner. This means that the tests always run in a Node environment and not in a real browser. This lets us enable fast iteration speed and prevent flakiness.

**Jest features:**

- **Instant Feedback:** Immersive Watch mode only runs test files related to changed files.
- **Fast and sandboxed:** It runs parallel tests across workers, and buffers console messages and prints them together.
- **Snapshot Testing:** Capture snapshots of React trees or other serializable values to simplify testing and analyze how state changes over time.
- **Built-in code coverage reports:** Supports --coverage for bringing out of the box coverage reports.
- **Zero configuration**

### Snapshot testing

Jest takes a snapshot of the component on first run that will save the output of a rendered component to file and it compares the component's output to the saved snapshot on subsequent runs. This is useful in knowing when your component changes its behaviour.

If you take a snapshot of a component the test passes on the first run but as soon as there is a change the test will fail because there'll be a mismatch between the component and its original "picture".

Snapshot tests are good for components that don't change often. You should write a snapshot test when the component is stabilized.

### Mocking

Mocking is a nice utility which excludes external component (eg, GoogleMap) from testing.

## About Enzyme

Enzyme provides a mechanism to mount and traverse React.js component trees. This will help you get access to its own properties and state as well as its children props in order to run your assertions. Enzyme has three methods for rendering React elements as below:

### Shallow rendering: shallow()

The shallow() function loads only the root component in memory.

It tests components in isolation from the child components they render. This is useful to isolate the component for pure unit testing.

Shallow rendering lets you render a component “one level deep” and assert facts about what its render method returns, without worrying about the behaviour of child components, which are not instantiated or rendered.

If a parent component renders another component that fails to render, then a shallow rendering on the parent will still pass.

It can access React _lifecycle_ methods.

**Shallow should be used for unit testing of a single isolated component.**

### Full rendering: mount()

The mount() function loads the full DOM tree.

Full DOM rendering including child components. It goes deeper and tests children of the component. It doesn't require an environment like a "browser".

Ideal for use cases where you have components that may interact with DOM API, or use React lifecycle methods in order to fully test the component.

As it actually mounts the component in the DOM _.unmount()_ should be called after each tests to stop tests affecting each other.

Allows access to both props directly passed into the root component (including default props) and props passed into child components.

It is more costly in execution time when using mount, because it requires JSDOM. JSDOM is a JavaScript based headless browser that can be used to create a realistic testing environment.

It can also access React _lifecycle_ methods.

### Static rendering: render()

Renders to static HTML, including children.

This is the only way to test componentDidMount and componentDidUpdate.

Less costly than _mount_ but provides less functionality.

It cannot access React _lifecycle_ methods.

Two of the most important assertion methods of Enzyme are **"simulate"** and **"find"**. The first one can simulate user events like click, hover, etc. The second method can find a child with a selector.

In React, **props** are received from a parent component and are read-only. If you want to change props in your component, you will get TypeError and need to use **"state"** instead.

From the performance point-of-view, there is practically no difference between using **class-based components** and using **function-based components**. ReactJS rendering mechanism is smart enough to optimize that for us.

**_We recommend always starting with Shallow for unit test of a single component. Use mount only when you need to test something related to the lifecycle. Render should be used when we want to test the children._**

It would be good, if you keep the display/UI concerns separate from program logic and side-effects, it makes your life a lot easier. This rule of thumb has always held true for me, in every language and every framework I’ve ever used.

In techincal practice, there are 3 main blocks are used to write any test: describe, it and expect.

- *describe* - is used to group tests that are related.

- *it* - makes an assertion to a fact about that test subject.

- *expect* - makes a single assertion about a specific case.

Minimal code for testing a single component is:

    import React, { useState } from "react";
    import ReactDOM from "react-dom";
    import { act } from "react-dom/test-utils";

    let container;

    beforeEach(() => {
      container = document.createElement("div");
      document.body.appendChild(container);
    });

    afterEach(() => {
      document.body.removeChild(container);
      container = null;
    });
    // test here

Minimal code for Snapshot testing:

    import App from '../App'

    describe('App component', () => {
      it('should shallow correctly', () => {
          expect(shallow(`<App />`)).toMatchSnapshot()
        })

      it('should mount correctly', () => {
          expect(mount(`<App />`)).toMatchSnapshot()
      })

      it('should render correctly', () => {
        expect(render(`<App />`)).toMatchSnapshot()
      })
    })

The API reduces boilerplate code.

    Shallow Rendering
    import { shallow } from 'enzyme';
    const wrapper = shallow(`<MyComponent />`);
    // ...

    Full Rendering
    import { mount } from 'enzyme';
    const wrapper = mount(`<MyComponent />`);
    // ...

    Static Rendering
    import { render } from 'enzyme';
    const wrapper = render(`<MyComponent />`);
    // ...
